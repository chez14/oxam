#!/bin/sh

cd backend

echo "Running composer install with --optimize-autoloader --no-dev..."
docker-compose run web composer update --optimize-autoloader --no-dev

echo "Post-pull script has successfully runned."