# Oxam - Examination Management App

Oxam is Examination Management App, made for Laboratorium Komputer of
[Department of Informatics](https://informatika.unpar.ac.id/), [Faculty of
Information Technology and Sciences](https://ftis.unpar.ac.id/). 

Oxam is currently made specifically for Labkom settings, where we have specific
room and schedule routine that has been proofed over years. Shall you have any
suggestions or questions, don't hesitate to create a new issue, PR is also
welcomed.

### What does it do?

- Create exam slot.
- Seat plotting with randomized picked position out of participant lists.
- LDAP Integration for gaining the name of the participant based on
  `sAMAccountName`.
- Generate batch script for Windows for distributing Exam necessities.
- Serve answer submission page with integrated timer for Projector and
  participant's computer.
- REST-based API for front-end necessities and other integration project.
- Auto-send answer to lecturer.

## Getting Started

### Installation

Installation can be done with docker, or bare-metaly. Please consult with your
hosting provider for more information before opening new issue ticket,
troubleshooting for your current setup.

All version mentioned here will follow [semantic
versioning](https://docs.npmjs.com/about-semantic-versioning#using-semantic-versioning-to-specify-update-types-your-package-can-accept).

#### Docker

We don't build docker image, just yet. To start, you will need to have your
server prepared with:

- Docker Engine (`>20.10`,
  [Installation](https://docs.docker.com/engine/install/))
- Docker Compose (`>1.17`,
  [Installation](https://docs.docker.com/compose/install/))

To start an instance, all you need to do is:

- Switch to `build/production` branch.

- Go to `backend/` folder.

- Copy `.env.production` to `.env` and fill in the necessary configuration, such
  as password and user configuration.

- Copy `docker-compose.prod.yml` to `docker-compose.yml`.

- From the `backend/` folder, go to `app/config/`.

- Copy `site.example.ini` to `site.ini` and fill all the required configuration.

- Run composer install on the docker by executing:

  ```bash
  $ docker-compose exec web composer install -- --no-dev
  ```



- Go back to `backend/` folder, and you can now run `docker-compose up`.

- Trigger the migration by accessing `http://your-instance-url/-/migrate`.

#### Bare Metal (Manual)

Signing up to manual installation means you'll have hard time updating and build
things. Just make sure you have following plugins and mods enabled and
functional:

- Composer (`^2.0`, Installation)
- PHP Ldap
- PHP Zip
- PHP Intl
- PHP cUrl

For the PHP plugin part, please consult with PHP's plugin manual as they're
updated frequently, and because i use wrappers, mostly any version newer than
specifiied on Composer  will solve those the rough API call.

Continuing on, to install, please make sure our top important configuration is
configured properly.

- These folders should be **writable**:

  - `backend/app/config/`
  - `backend/logs/`
  - `backend/vendor/`
  - `backend/_tmp/`

- Make sure **only this folder** that **can be accessible** from public. This
  folder is purposed entirely for root document, and shall be treated as so. All
  of our "secured content" are lies on the `app/` folder. Shall that folder
  accessible by public directly, consider your installation as insecure. Of
  course a fall-back plan has been [implemented](backend/app/.htaccess), but
  that workaround only works on Apache, not Nginx. So make sure you configure it
  properly. **You've been warned.**

  - `backend/public_html/`

- The App configuration should be initialized:

  - From the `backend/` folder, go to `app/config/`.

  - Copy `site.example.ini` to `site.ini` and fill all the required
    configuration.

  - Go back to `backend/` folder.

  - Install all the dependencies by executing `composer install --no-dev` on
    `backend/` folder you're currently on.

    ```bash
    $ composer install --no-dev
    ```

- Trigger the migration. To trigger the database migration, all you need to do
  is accessing http://your-installation-url/-/migrate`.

- Building the front-end. You can skip this by switching it to
  `build/production` branch. But if you insist to build it by your self, you can
  do it by:

  - Go to `front-end/` folder.

  - Execute `yarn`

  - Execute build script with this rather specific build command:

    ```bash
    $ REACT_APP_BUILD_DATE=$(date +"%F %T") yarn run build
    ```

  - Copy the contents of `front-end/build/` folder to `back-end/public_html`.

Shall you have any questions, please consult our [build
pipeline](.gitlab-ci.yml) first before asking on issue.

### Development Environment

You'll need to have this up and running:

- Docker (optional, recommended)
- Docker Compose (optional, recommended)
- Composer
- Apache/Nginx, PHP, MySQL/MariaDB, and this mods and plugins: This should be
  bundled automatically when you use this project's Docker image.
  - cUrl
  - php-curl
  - php-pdo
  - php-ldap
  - php-zip
  - php-intl
- NodeJs
- Yarn
- Git, obviously.



## License

While the creator (Chris) aspire for open source, we usually comply with the
projects that have stricter license. If there's none, we'll usually put them on
MIT License. Please check both project as this repo contains multiple different
license.

## Additional Information

This project are created as part of Gunawan Christianto (2016730011)'s
Undegraduate Thesis with the help of [Raymond Chandra Putra, S.T.,
M.T.](https://informatika.unpar.ac.id/dosen/raymond-chandra/) as my Dosen
Pembimbing. You can check the documents
[here](https://github.com/chez14/undergrad-thesis).